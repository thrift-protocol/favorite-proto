// 商品收藏服务
namespace go favoriteproto
namespace java shop.yumall.thrift.favorite
namespace php Yumall.Thrift.Favorite

const string SERVER_NAME = "yu-favorite-server";

struct Favorite {
    1: required string sku;
    2: required string userId;
}

service FavoriteService {
    list<string> get(1: string userId);
    bool add(1: Favorite favorite);
    bool remove(1: Favorite favorite);
}