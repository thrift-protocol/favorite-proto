#!/usr/bin/env bash

rm -rf ./gen-*
rm -rf ./src/main/*
rm -rf ./php-src/*
rm -rf ./favoriteproto

thrift -r --gen php:server yumall.thrift.favorite.thrift && \
mv gen-php/* php-src/

thrift -r --gen java yumall.thrift.favorite.thrift && \
mv gen-java/* src/main/java

thrift -r --gen go yumall.thrift.favorite.thrift && \
mv gen-go/* ./

rm -rf ./gen-*